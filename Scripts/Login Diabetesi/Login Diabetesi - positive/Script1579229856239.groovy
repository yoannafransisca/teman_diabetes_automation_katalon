import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Memulai Aplikasi TD
Mobile.startApplication('C:\\Users\\asus\\Documents\\KULIAH\\MAGANG/TemanDiabetes(1).apk/', false)

//Menunggu Element Masukkan Email
Mobile.waitForElementPresent(findTestObject('Login Non Diabetesi/Add email'), 4)

//Memasukkan Email yang sudah terdaftarkan 
Mobile.setText(findTestObject('Login Non Diabetesi/Add email'), 'dwachyono@gmail.com', 0)

//Menunggu Element Masukkan Password
Mobile.waitForElementPresent(findTestObject('Login Non Diabetesi/addPassword'), 3)

//Memasukkan Password
Mobile.setText(findTestObject('Login Non Diabetesi/addPassword'), 'Yoanna123', 0)

//Tap button Login atau Masuk
Mobile.tap(findTestObject('Login Non Diabetesi/btnMasuk'), 0)

//Menunggu Login 
Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

//Melihat profile sebagai user diabetesi
Mobile.tap(findTestObject('Login Diabetesi/BtnProfile'), 0)

