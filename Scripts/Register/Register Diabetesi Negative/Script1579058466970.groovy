import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('C:\\Users\\asus\\Documents\\KULIAH\\MAGANG/TemanDiabetes(1).apk/', false)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Register/Buat Akun', [('variable') : '']), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Register/Fullname'), '', 0)

Mobile.tap(findTestObject('Register/btn Lanjut'), 0)

Mobile.getElementTopPosition(findTestObject('Register/AlertMsg - Masukkan nama Anda', [('flowControl') : '']), 0)

Mobile.setText(findTestObject('Register/Fullname'), 'Meiliani', 0)

Mobile.tap(findTestObject('Register/btn Lanjut'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Register/Add Email'), '', 0)

Mobile.tap(findTestObject('Register/btn Lanjut'), 0)

Mobile.getElementTopPosition(findTestObject('Register/AlertMsg- Masukan email Anda'), 0)

Mobile.setText(findTestObject('Register/Add Email'), 'annonymousdota2@gmail.com', 0)

Mobile.tap(findTestObject('Register/btn Lanjut'), 0)

Mobile.setText(findTestObject('Register/Password'), '', 0)

Mobile.setText(findTestObject('Register/Password Review'), '', 0)

Mobile.tap(findTestObject('Register/btn Lanjut'), 0)

Mobile.getElementTopPosition(findTestObject('Register/AlertMsg - Silakan lengkapi semua isian'), 0)

Mobile.setText(findTestObject('Register/Password'), 'Test123', 0)

Mobile.setText(findTestObject('Register/Password Review'), 'Test123', 0)

Mobile.tap(findTestObject('Register/btn Lanjut'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Register/btn Selesai'), 0)

Mobile.waitForElementPresent(findTestObject('Register/Alert User'), 3)

Mobile.tap(findTestObject('Register/btnOK'), 0)

