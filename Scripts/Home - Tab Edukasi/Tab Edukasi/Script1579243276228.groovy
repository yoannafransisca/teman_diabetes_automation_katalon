import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Memulai Aplikasi TD
Mobile.startApplication('C:\\Users\\asus\\Documents\\KULIAH\\MAGANG/TemanDiabetes(1).apk/', false)

//Menunggu Membuka halaman Home
Mobile.waitForElementPresent(findTestObject('Home - Tab Edukasi/tab Edukasi'), 3)

//Tap button Edukasi
Mobile.tap(findTestObject('Home - Tab Edukasi/tab Edukasi'), 0)

//Menunggu tap detail apa itu diabetes
Mobile.waitForElementPresent(findTestObject('Home - Tab Edukasi/Detail Apa itu Diabetes'), 3)

//tap detail apa itu diabetes
Mobile.tap(findTestObject('Home - Tab Edukasi/Detail Apa itu Diabetes'), 0)

//Menunggu membuka page detail apa itu diabetes
Mobile.waitForElementPresent(findTestObject('Home - Tab Edukasi/scroll view'), 3)

//Scroll page detail apa itu diabetes
Mobile.swipe(563, 2000, 563, 300)

//Menunggu element button back 
Mobile.waitForElementPresent(findTestObject('Home - Tab Edukasi/btnBack'), 5)

//Tap Button Back untuk kembali ke halaman Edukasi
Mobile.tap(findTestObject('Home - Tab Edukasi/btnBack'), 0)

