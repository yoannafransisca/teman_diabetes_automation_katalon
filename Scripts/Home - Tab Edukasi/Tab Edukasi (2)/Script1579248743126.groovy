import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('C:\\Users\\asus\\Documents\\KULIAH\\MAGANG/TemanDiabetes(1).apk/', false)

//Menunggu Membuka halaman Home
Mobile.waitForElementPresent(findTestObject('Home - Tab Edukasi/tab Edukasi'), 3)

//Tap button Edukasi
Mobile.tap(findTestObject('Home - Tab Edukasi/tab Edukasi'), 0)

Mobile.swipe(930, 1600, 930, 500)

Mobile.waitForElementPresent(findTestObject('Home - Tab Edukasi (2)/Tap Mengurangi resiko komp'), 5)

Mobile.swipe(950, 1800, 53, 1900)

Mobile.tap(findTestObject('Home - Tab Edukasi (2)/Tap Mengurangi resiko komp'), 0)

Mobile.swipe(898, 1900, 981, 300)

Mobile.tap(findTestObject('Home - Tab Edukasi (2)/btnBack'), 0)

