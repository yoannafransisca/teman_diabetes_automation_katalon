import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.github.javafaker.Faker as Faker

Faker faker = new Faker()

String name = faker.name().fullName()

String firstName = faker.name().firstName()

String email = firstName + '@yopmail.com'

println(firstName)

Mobile.startApplication('C:\\Users\\asus\\Documents\\KULIAH\\MAGANG/TemanDiabetes(1).apk/', false)

Mobile.waitForElementPresent(findTestObject('Forget Password/Lupa Password', [('variable') : '']), 3)

Mobile.tap(findTestObject('Forget Password/Lupa Password'), 0)

Mobile.waitForElementPresent(findTestObject('Forget Password/android.widget.EditText0 - masukkan email anda'), 3)

Mobile.setText(findTestObject('Forget Password/android.widget.EditText0 - masukkan email anda'), email, 0)

Mobile.tap(findTestObject('Forget Password/btnKirim'), 0)

Mobile.waitForElementPresent(findTestObject('Forget Password/Alert Email Salah'), 3)

