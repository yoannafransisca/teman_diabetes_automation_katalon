import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Memulai Aplikasi TD
Mobile.startApplication('C:\\Users\\asus\\Documents\\KULIAH\\MAGANG/TemanDiabetes(1).apk/', false)

//Menunggu Element Memasukkan Email 
Mobile.waitForElementPresent(findTestObject('Login Non Diabetesi/Add email'), 3)

//Memasukkan Email yang sudah didaftarkan
Mobile.setText(findTestObject('Login Non Diabetesi/Add email'), 'yoannafransisca9912@gmail.com', 0)

//Menunggu Element Memasukkan Password
Mobile.waitForElementPresent(findTestObject('Login Non Diabetesi/addPassword'), 3)

//Memasukkan Password
Mobile.setText(findTestObject('Login Non Diabetesi/addPassword'), 'Yoanna123', 0)

//Tap Button Login atau Masuk
Mobile.tap(findTestObject('Login Non Diabetesi/btnMasuk'), 0)

//Menunggu User Login
Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

//Mengecek login user sebagai non diabetesi
Mobile.tap(findTestObject('Login Non Diabetesi/BtnProfile'), 0)

